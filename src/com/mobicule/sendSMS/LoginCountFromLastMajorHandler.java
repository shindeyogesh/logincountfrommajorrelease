package com.mobicule.sendSMS;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.PumpStreamHandler;

import com.mobicule.dao.LoginCountFromLastMajor;

public class LoginCountFromLastMajorHandler {

	public void sendSMS() throws SQLException {
		Properties properties = new Properties();
		InputStream stream = null;
		try {
			stream = new FileInputStream("/home/msales/msales_script/files_jar/VConnectAlerts/MSISDN.properties");
			properties.load(stream);
			String[] msisdnList = properties.getProperty("LoginCountFromLastMajor_MSISDN").split(",");
			String majorVersion = properties.getProperty("MajorVersion");
			Map<String, String> loginCountsFromLastMajor = LoginCountFromLastMajor
					.loginCountFromLastMajor(majorVersion);
			String message = "Login Count From Last Major:";
			if (loginCountsFromLastMajor.size() > 0) {
				for (Map.Entry<String, String> entry : loginCountsFromLastMajor.entrySet())
					message += "\n" + entry.getKey() + ":" + entry.getValue();
			} else
				message = "No any login counts found";
			System.out.println("Final message is :\n" + message);
			byte b;
			int i;
			String[] arrayOfString1;
			for (i = (arrayOfString1 = msisdnList).length, b = 0; b < i;) {
				String msisdn = arrayOfString1[b];
				System.out.println("Sending message to mobile number : " + msisdn);
				smsOTPViaPerl(msisdn, message);
				b++;
			}
		} catch (Exception e) {
			System.out.println("Exception while sending message" + e.getStackTrace());
			e.printStackTrace();
		}
	}

	private static void smsOTPViaPerl(String mobileNumber, String Message) {
		try {
			String messageTobeSent = Message;
			String absoluteFilePath = "/home/msales/msales_script/files_jar/Sms_Script/push.pl";
			String command = "perl " + absoluteFilePath + " " + mobileNumber + " " + messageTobeSent;
			System.out.println("command is : " + command);
			ByteArrayOutputStream stdout = new ByteArrayOutputStream();
			PumpStreamHandler pumpStreamHandler = new PumpStreamHandler(stdout);
			CommandLine cl = CommandLine.parse(command);
			DefaultExecutor exec = new DefaultExecutor();
			exec.setStreamHandler((ExecuteStreamHandler) pumpStreamHandler);
			exec.execute(cl);
		} catch (Exception e) {
			System.out.println("Exception while calling perl script : " + e.getStackTrace());
			e.printStackTrace();
		}
	}
}
