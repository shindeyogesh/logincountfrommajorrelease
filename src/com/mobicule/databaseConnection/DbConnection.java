package com.mobicule.databaseConnection;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DbConnection {
  public static Connection getDBConnection() throws IOException {
    Properties prop = new Properties();
    InputStream input = null;
    input = new FileInputStream("/home/msales/msales_script/files_jar/VConnectAlerts/NDC3_DB_Connection_Details.properties");
    prop.load(input);
    String username = prop.getProperty("username");
    String password = prop.getProperty("password");
    String url = prop.getProperty("JDBCURL");
    Connection con = null;
    try {
      Class.forName("oracle.jdbc.driver.OracleDriver");
      con = DriverManager.getConnection(url, username, password);
    } catch (Exception e) {
      System.out.println(e);
    } 
    return con;
  }
}