package com.mobicule.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.mobicule.databaseConnection.DbConnection;

public class LoginCountFromLastMajor {

	public static Map<String, String> loginCountFromLastMajor(String majorVersion) throws SQLException {
		Map<String, String> loginCount = new LinkedHashMap<>();
		Connection connection = null;
		try {
			String SQL_Query = String.format(
					"SELECT version_code as versionCode, Count(*) as loginCount FROM EKYC_LIVE.user_details WHERE version_code >= %s AND modified_on BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400 GROUP BY version_code ORDER BY version_code asc",
					majorVersion);
			System.out.println("Login Count From Last Major Version Code Wise Query : " + SQL_Query);
			connection = DbConnection.getDBConnection();
			PreparedStatement statement = connection.prepareStatement(SQL_Query);
			ResultSet result = statement.executeQuery();
			while (result.next())
				loginCount.put(result.getString(1), result.getString(2));
			System.out.println("Login Count : " + loginCount.toString());
		} catch (Exception localException) {
			System.out.println("Local Exception : " + localException);
			System.out.println("git");
		} finally {
			connection.close();
		}
		return loginCount;
	}
}
